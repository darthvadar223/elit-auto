<header>
    <div id="main-slider">
        <?php
        $data = json_decode(file_get_contents("img/main-slider/order.json"), true);
            foreach ($data as $row):
                ?>
                <img src="img/main-slider/<?=$row?>" alt="слайд">
            <?php
            endforeach;
            ?>
    </div>
    <div class="wrapper">
        <div class="header-top">
            <div id="ht-left">
                <?php
                include "logo.php";
                include "navigation.php";
                ?>
            </div>
            <div id="ht-right">
                <p><span>+380 (98) 495-35-95</span></p>
                <p>проспект Никопольский, 24</p>
                <p>Мариуполь</p>
            </div>
        </div>
        <h1>Ремонт и техобслуживание автомобиля в Мариуполе</h1>
        <p>Гарантия на ремонт и запасные запчасти</p>
        <p>Честность: меняем только то, что нужно менять</p>
        <p>Выполняем ремонт в оговоренные сроки</p>
        <div class="button"><a class="anchor" href="#services-section">Смотреть услуги</a></div>
        <div class="dots"></div>
    </div>
</header>