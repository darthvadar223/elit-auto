//autosize(document.getElementById("autosize"));

$("#leave-benefit").click(function () {
    $("body").css("overflow","hidden");
    $("#benefit-overlay")
        .css("visibility", "visible")
        .css("opacity", "1")
        .children("div")
        .css("transform", "scale(1)");
});

function benefit_overlay_close() {
    $("body").css("overflow","auto");
    $("#benefit-overlay")
        .css("opacity", "0")
        .children("div")
        .css("transform", "scale(0)");

    setTimeout(function () {
        $("#benefit-overlay")
            .css("visibility", "hidden")
    }, 300);
}

$("#benefit-overlay>div>div>img")
    .click(function () {
        benefit_overlay_close();
    });
$("#benefit-overlay")
    .click(function (e) {
        if(e.target === this) {
            benefit_overlay_close();
        }
    });

$("#benefit-form>.estimate>.stars>img").mouseenter(function () {
    $fill_count = $(this).index();
    for($i = 0; $i < 5; $i++) {
        if($fill_count >= $i) {
            $($("#benefit-form>.estimate>.stars>img")[$i]).addClass("hovered");
        } else {
            $($("#benefit-form>.estimate>.stars>img")[$i]).removeClass("hovered");
        }
    }
});

$("#benefit-form>.estimate>.stars>img").mouseleave(function () {
    $(".hovered").removeClass("hovered");
});

bind();
function bind() {
    $("#benefit-form>.estimate>.stars>img").click(function () {
        $("#benefit-form").children("span").css("visibility", "hidden");
        $(this).parent().siblings("label").css("color", "#0bca95");
        $fill_count = $(this).index();
        $($("#benefit-form>.estimate>input")).val($fill_count);
        $stars = "";
        for($i = 0; $i < 5; $i++) {
            if($fill_count >= $i) {
                $stars += `<img src="img/star.svg" alt="звезда">`;
            } else {
                $stars += `<img src="img/star-empty.svg" alt="пустая звезда">`;
            }
        }
        $(this).parent().addClass("selected").html($stars);
        bind();
    });
}

$("#benefit-form>.file>input").change(function () {
    $("#benefit-form").children("span").css("visibility", "hidden");
    if(this.files.length > 0) {
        $parts = this.files[0].name.split('.');
        $name = $parts[$parts.length - 1];
        if($name.toLowerCase() === "jpg") {
            $(this).parent().children("div").text(this.files[0].name);
        } else {
            $(this).parent().siblings("span").text("Картинка должна быть в формате JPG").css("visibility", "visible");
        }
    } else {
        $(this).parent().children("div").text("Прикрепить фото");
    }
});

$("#benefit-form input, #benefit-form textarea").focus(function () {
    $("#benefit-form").children("span").css("visibility", "hidden");
    $(this)
        .css("border-color", "#0bca95");
});

$("#benefit-form input, #benefit-form textarea").focusout(function () {
    if($(this).val() === "") {
        $(this)
            .css("border-color", "#d90606")
            .siblings("label")
            .css("color", "#d90606");
    } else {
        $(this)
            .css("border-color", "#0bca95")
            .siblings("label")
            .css("color", "#0bca95");
    }
});

$("#benefit-form input, #benefit-form textarea").bind("paste", function (e) {
    e.preventDefault();
});

$("#benefit-form input, #benefit-form textarea, .stars>img").on("input click", function () {
    if(check_fields()) {
        if($("#submit").hasClass("disabled"))
            $("#submit").removeClass("disabled");
    } else {
        if(!($("#submit").hasClass("disabled")))
            $("#submit").addClass("disabled");
    }
});

$("#benefit-form textarea").on("keydown", function (e) {
    if(e.which === 13) {
        e.preventDefault();
        $("#benefit-form").submit();
    }
});

function check_fields($is_submit = false) {
    $length = $("#benefit-form input").length - 2;
    $filled_fields = true;
    for($i = 0; $i < $length; $i++) {
        if($($("#benefit-form input")[$i]).val() === "") {
            if($filled_fields)
                $filled_fields = false;
            if($is_submit) {
                $($("#benefit-form input")[$i])
                    .css("border-color", "#d90606")
                    .siblings("label")
                    .css("color", "#d90606");
            }
        }
    }
    if($("#benefit-form textarea").val() === "") {
        if($filled_fields)
            $filled_fields = false;
        if($is_submit) {
            $("#benefit-form textarea")
                .css("border-color", "#d90606")
                .siblings("label")
                .css("color", "#d90606");
        }
    }
    return $filled_fields;
}

$("#benefit-form").submit(function (e) {
    e.preventDefault();
    if(check_fields(true)) {
        $formdata = new FormData($(this).get(0));
        $.ajax({
            url: "ajax/benefit.php",
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data: $formdata,
            success: function (data) {
                if(data.error) {
                    alert("Произошла ошибка, повторите попытку позже");
                } else {
                    $("#benefit-overlay>div>div").append("<h2 style='font-size: 36px;\n" +
                        "    font-weight: 500;\n" +
                        "    text-align: center;\n" +
                        "    display: none;" +
                        "    color: #0bca95;'>Отправлен</h2>");
                    $("#benefit-form").slideToggle(1000, function () {
                        $("#benefit-overlay>div>div")
                            .children("h2")
                            .slideToggle(500);
                        $(this).remove();
                    });
                }
            },
            error: function (data) {
                alert("Произошла ошибка, повторите попытку позже");
            },
            complete: function () {
            }
        });
    } else {
        $(this).children("span").text("Пожалуйста, заполните пустые поля").css("visibility", "visible");
    }
    return false;
});