$length = $("#main-slider>img").length;

if($length > 0) {
    $($("#main-slider>img")[0]).addClass("main-active");
    for ($i = 0; $i < $length; $i++) {
        $(".dots").append("<div></div>");
    }
    $($(".dots>div")[0]).addClass("dot-active");

    $interval = setInterval(loop, 5000);

    $(".dots>div").click(function () {
        clearInterval($interval);
        $interval = setInterval(loop, 5000);
        $main_next_index = $(this).index();
        main_next_slide($main_next_index);
    });

    function main_next_slide($main_next_index) {
        $(".dot-active")
            .removeClass("dot-active");
        $($(".dots>div")[$main_next_index])
            .addClass("dot-active");
        $(".main-active")
            .removeClass("main-active");
        $($("#main-slider>img")[$main_next_index])
            .addClass("main-active");
    }

    function loop() {
        $current_index = $(".dots>div.dot-active").index();
        $main_next_index = $current_index === $(".dots>div").length - 1 ?
            0 :
            $current_index + 1;
        main_next_slide($main_next_index);
    }
}
