$length = $(".images>div>img").length;

if($length > 0) {
    $($(".images>div>img")[0]).addClass("image-active");
    for ($i = 0; $i < $length; $i++) {
        $(".dots").append("<div></div>");
    }

    $($(".dots>div")[0]).addClass("active");

    $("#left-arrow").click(function () {
        $active_index = $(".images")
            .children("div")
            .children("img")
            .index($(".image-active"));
        $next_index = $active_index === 0 ?
            $(".images")
                .children("div")
                .children("img").length - 1 :
            $active_index - 1;
        next_slide($next_index);
    });

    $("#right-arrow").click(function () {
        $active_index = $(".images")
            .children("div")
            .children("img")
            .index($(".image-active"));
        $next_index = $active_index === $(".images")
            .children("div")
            .children("img").length - 1 ?
            0 :
            $active_index + 1;
        next_slide($next_index);
    });

    $(".dots>div").click(function () {
        $next_index = $(this).index();
        next_slide($next_index);
    });

    function next_slide($next_index) {
        $(".active")
            .removeClass("active");
        $($(".dots>div")[$next_index])
        //        .children("div")[$next_index]
            .addClass("active");
        $(".image-active")
            .removeClass("image-active");
        $($(".images>div>img")[$next_index])
            .addClass("image-active");
        $(".images")
            .children("div")
            .css("margin-left", "-" + $next_index + "00%");
    }
}