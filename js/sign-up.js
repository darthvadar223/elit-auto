$(".sign-up").click(function () {
    $("body").css("overflow","hidden");
    $("#overlay")
        .css("visibility", "visible")
        .css("opacity", "1")
        .children(".shadow")
        .css("transform", "scale(1)");
});

function overlay_close() {
    $("body").css("overflow","auto");
    $("#overlay")
        .css("opacity", "0")
        .children(".shadow")
        .css("transform", "scale(0)");

    setTimeout(function () {
    $("#overlay")
        .css("visibility", "hidden")
    }, 300);
}

$("#overlay>div>div>img")
    .click(function () {
        overlay_close();
    });
$("#overlay")
    .click(function (e) {
        if(e.target === this) {
            overlay_close();
        }
    });