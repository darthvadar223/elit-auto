function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

$length = $(".random-color").length;

for($i = 0; $i < $length; $i++) {
    $($(".random-color")[$i]).text($($(".random-color")[$i]).parent().parent().siblings(".benefit-container").children(".info").children("span").text()[0]).parent()
        .css("background-color", getRandomColor());
}