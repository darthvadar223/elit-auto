function unbind() {
    $("aside>.menu>li>ul>li").unbind();
    $("aside>.menu>.default>a>div").unbind();
    $("aside>.menu>.dropdown>a>div").unbind();
    $("a.navigate").unbind();
}

function bind() {
    $("aside>.menu>.dropdown>a").click(function () {
        $focused = $(this).parent().hasClass("focused");
        $focused_selector = $(".focused");
        if (!$focused)
            $focused_selector.children("ul").slideToggle(700);
        $focused_selector.removeClass("focused");
        if (!$focused)
            $(this).parent().children("ul").slideToggle(700);
        $(this).parent().addClass("focused");
    });

    $("aside>.menu>.default>a").click(function () {
        $focused_selector = $(".focused");
        $focused_selector
            .children("ul")
            .slideToggle(700);
        $focused_selector.removeClass("focused");
        $(".selected")
            .removeClass("selected");
        $(".nested-selected")
            .removeClass("nested-selected");
        $(this).parent()
            .addClass("focused")
            .addClass("selected");
    });

    $("aside>.menu>li>ul>li>a").click(function () {
        $(".selected").removeClass("selected");
        $(".nested-selected").removeClass("nested-selected");
        $(this).parent().addClass("nested-selected");
    });

    $("a.navigate").click(function (e) {
        unbind();
        if(!e.isTrigger) {
            $("#menu").click();
        }
        ajax($(this).attr("name"));
    });
}

function ajax($name) {
    $(".overlay").addClass("visible");

    $.ajax({
        url: "ajax/admin-bars.php",
        type: "post",
        data: "name=" + $name,
        success: function ($json) {
            $data = $.parseJSON($json);
            setContent($data);
        },
        error: function ($data) {
        },
        complete() {
            $(".overlay").removeClass("visible");
            bind();
        }
    });
}

ajax("start-page");

function setContent($data) {
    $("#aside-right>.top>h1").text($data["h1"]);
    $("#aside-right>.top>h2").text($data["h2"]);
    $("#aside-right>.bottom").html($data["content"]);
}

$("#applications").click(function () {
    $("ul.menu>li.default>a[name='applications']").trigger("click");
});

$("#benefits").click(function () {
    $("ul.menu>li.default>a[name='benefits']").trigger("click");
});

$("#settings").click(function () {
    $("ul.menu>li.default>a[name='settings']").trigger("click");
});

function check_options() {
    $.ajax({
        url: "../ajax/check-options.php",
        type: "post",
        success: function ($json) {
            $data = $.parseJSON($json);
            if(parseInt($data["application_count"]) > 0) {
                if($("#applications").children("div").length) {
                    $("#applications").children("div").text($data["application_count"]);
                } else {
                    $("#applications").append("<div class='counter'>"+$data["application_count"]+"</div>")
                }
            } else {
                if($("#applications").children("div").length) {
                    $("#applications").children("div").remove();
                }
            }
            if(parseInt($data["benefit_count"]) > 0) {
                if($("#benefits").children("div").length) {
                    $("#benefits").children("div").text($data["benefit_count"]);
                } else {
                    $("#benefits").append("<div class='counter'>"+$data["benefit_count"]+"</div>")
                }
            } else {
                if($("#benefits").children("div").length) {
                    $("#benefits").children("div").remove();
                }
            }
        }
    });
}
check_options();

setInterval(check_options, 10000);

