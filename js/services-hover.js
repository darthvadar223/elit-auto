$hovered_class = "";
$(".hover-1, .hover-2, .hover-3").mouseenter(function () {
    $class_name = $(this)[0].className;
    if($hovered_class !== $class_name) {
        if($hovered_class === "") {
            $hovered_class = $class_name;
        }
        $("div."+$hovered_class+">a")
            .finish()
            .css("margin-top", "-50px")
            .css("opacity", "0");
        $hovered_class = $class_name;
        $("div." + $(this)[0].className + ">a")
            .finish()
            .css("margin-top", "50px")
            .css("opacity", "1");
    }
});

$("#hovers").mouseleave(function () {
    $("div."+$hovered_class+">a")
        .finish()
        .css("margin-top", "-50px")
        .css("opacity", "0");
    $hovered_class = "";
});
