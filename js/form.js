$(".main-form").submit(function(e) {
    e.preventDefault();

    $query = $(this)
        .children("div")
        .children("input[name=phone]");

    if ($query.val() === "") {
        $query.parent().addClass("error");
        $(this)
            .children("div")
            .children("span")
            .text("Укажите свой номер телефона");
        return false;
    }
    if ($query.val().replace(/[^0-9]/g, '').length < 10) {
        $query.parent().addClass("error");
        $(this)
            .children("div")
            .children("span")
            .text("Укажите телефон в формате 0(00)-000-00-00");
        return false;
    }
    $.ajax({
        url: "ajax/forms.php",
        type: "POST",
        data: $(this).serialize(),
        success: function (data) {
            $(".form").children(".form-default").remove();
            $(".form")
                .append(`
                        <div class="form-success">
                            <img src="img/form/success.svg" alt="успех">
                            <h1 class="success">Заявка отправлена</h1>
                            <div>
                                <p>Ожидайте нашего ответа.</p>
                                <p>Мы свяжемся с Вами в ближайшее время.</p>
                            </div>
                        </div>
                `);
        },
        error: function (data) {
            $(".form").children(".form-default").remove();
            $(".form")
                .append(`
                        <div class="form-error">
                            <img src="img/form/database.svg" alt="ошибка">
                            <h1 class="error">Упс… на сервере ошибка</h1>
                            <div>
                                <p>Произошла ошибка на сервере.</p>
                                <p><a id="refresh">Обновите</a> страницу и заполните
                                    форму заново</p>
                            </div>
                        </div>`
                );
        },
        complete: function () {
            $("#refresh").click(function () {
                location.reload();
            });
        }
    });
    return;
});

$options =  {
    onChange: function(){
        $query = $(".main-form")
            .children("div")
            .children("input[name=phone]");
        if($query.parent().hasClass("error")){
            $query.parent().removeClass("error");
        }
    }
};

$(".main-form")
    .children("div")
    .children("input[name=phone]").mask("0(99)-999-99-99", $options);
