$("#menu").click(function () {
    if($(this).is(":not(:hidden)")) {
        if ($(this).hasClass("menu-closed")) {
            $("#aside-left").css("left", "0");
            $(this).css("left", "265px")
                .css("transform", "rotateY(180deg)")
                .removeClass("menu-closed")
                .addClass("menu-opened");
        } else if ($(this).hasClass("menu-opened")) {
            $("#aside-left").css("left", "-250px");
            $(this).css("left", "15px")
                .css("transform", "rotateY(0)")
                .removeClass("menu-opened")
                .addClass("menu-closed");
        }
    }
});