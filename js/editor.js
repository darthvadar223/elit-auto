function bind_editor() {
    $(".text-editor>div").sortable({axis: "y", handle: ".handle"});
    $(".text-editor .drag>div>div").sortable({axis: "y", handle: ".nested-handle"});
    $(".list-drag").sortable({axis: "y", handle: ".nested-handle-list"});

    $("[contenteditable='true']").on("keydown", function (e) {
        if (e.keyCode === 13) {
            $prop = $(this).prop("tagName").toLowerCase();
            if (e.shiftKey && $prop !== "h1" && $prop !== "h2") {
                document.execCommand("defaultParagraphSeparator", false, "br");
            } else {
                e.preventDefault();
                if ($(this).text().trim() !== "") {
                    if ($prop === "p") {
                        $selector = $(`<div class='nested-drag'>
                <span class='nested-handle'><i class='fa fa-bars' aria-hidden='true'></i></span>
                <` + $prop + ` class="content" contenteditable='true'>` + $(this).html() + `</` + $(this).prop("tagName") + `>
                <span class='remove'><i class='fa fa-times' aria-hidden='true'></i></span>
            </div>`).insertBefore($(this).parent());
                        $(this).html("");
                    } else if ($prop === "span") {
                        $selector = $(`
                                <li>
                                    <span class='nested-handle-list'><i class='fa fa-bars' aria-hidden='true'></i></span>
                                    <`+$prop+` contenteditable='true'>`+$(this).html()+`</`+$prop+`>                            
                                    <span class='remove'><i class='fa fa-times' aria-hidden='true'></i></span>
                                </li>`)
                            .insertBefore($(this).parent());
                        $(this).html("");
                    }
                    refresh_editor();
                }
            }
        }
    });

    $(".new-block").on("click", function () {
        $closest = $(this).closest(".text-editor");
        $children = $closest.children("div").children(".drag");
        $html = `<div class='drag'>
            <span class='handle'><i class='fa fa-bars' aria-hidden='true'></i></span>
            <div>
                <h1 class="content" contenteditable='true'>Заголовок</h1>
                <div>
                    <div class='nested-drag'>
                        <span class='nested-handle'><i class='fa fa-bars' aria-hidden='true'></i></span>
                        <p class="content" contenteditable='true'>Абзац</p>
                        <span class='remove'><i class='fa fa-times' aria-hidden='true'></i></span>
                    </div>
                </div>
                <ul class='options'>
                    <li>
                        <button class='paragraph'>Абзац</button>
                    </li>
                    <li>
                        <button class='list'>Список</button>
                    </li>
                    <li>
                        <button class='subtitle'>Подзаголовок</button>
                    </li>
                    <li>
                        <button class='photo'>Фото</button>
                    </li>
                </ul>
            </div>
            <span class='remove'><i class='fa fa-times' aria-hidden='true'></i></span>
        </div>`;
        if ($children.length > 0) {
            $($html).insertAfter($children.last());
        } else {
            $closest.children("div").prepend($html);
        }
        refresh_editor();
    });

    $("[contenteditable='true']").on("paste", function (e) {
        e.preventDefault();
        document.execCommand("insertText", true, e.originalEvent
            .clipboardData.getData('text/plain').replace(/\s+/g, " "));
    });

    $(".remove").on("click", function () {
        if (confirm("Вы уверены что хотите удалить этот элемент?")) {
            if($(this).parent().prop("tagName").toLowerCase() === "li") {
                $parent = $(this).parent();
                if ($parent.parent().children("li").length === 1) {
                    $parent.parent().parent().remove();
                    return true;
                }
            }
            $(this).parent().remove();
        }
    });

    $(".paragraph").on("click", function () {
        $(this).closest("div").children("div").append(`
                    <div class='nested-drag'>
                        <span class='nested-handle'><i class='fa fa-bars' aria-hidden='true'></i></span>
                        <p class="content" contenteditable='true'>Абзац</p>
                        <span class='remove'><i class='fa fa-times' aria-hidden='true'></i></span>
                    </div>`);
        refresh_editor();
    });

    $(".list").on("click", function () {
        $(this).closest("div").children("div").append(`
                        <div class='nested-drag'>
                            <span class='nested-handle'><i class='fa fa-bars' aria-hidden='true'></i></span>
                            <ul class='list-drag content'>
                                <li>
                                    <span class='nested-handle-list'><i class='fa fa-bars' aria-hidden='true'></i></span>
                                    <span contenteditable='true'>Список</span>                            
                                    <span class='remove'><i class='fa fa-times' aria-hidden='true'></i></span>
                                </li>
                            </ul>
                            <span class='remove'><i class='fa fa-times' aria-hidden='true'></i></span>
                        </div>`);
        refresh_editor();
    });

    $(".subtitle").on("click", function () {
        $(this).closest("div").children("div").append(`<div class='drag'>
            <span class='nested-handle'><i class='fa fa-bars' aria-hidden='true'></i></span>
            <div>
                <h2 class="content" contenteditable='true'>Подзаголовок</h2>
                <div>
                    <div class='nested-drag'>
                        <span class='nested-handle'><i class='fa fa-bars' aria-hidden='true'></i></span>
                        <p class="content" contenteditable='true'>Абзац</p>
                        <span class='remove'><i class='fa fa-times' aria-hidden='true'></i></span>
                    </div>
                </div>
                <ul class='options'>
                    <li>
                        <button class='paragraph'>Абзац</button>
                    </li>
                    <li>
                        <button class='list'>Список</button>
                    </li>
                </ul>
            </div>
            <span class='remove'><i class='fa fa-times' aria-hidden='true'></i></span>
        </div>`);
        refresh_editor();
    });
}

function refresh_editor() {
    $("[contenteditable='true']").unbind();
    $(".remove").unbind();
    $(".new-block").unbind();
    $(".paragraph").unbind();
    $(".list").unbind();
    $(".subtitle").unbind();
    bind_editor();
}

bind_editor();

if($(".text-editor>div").html().trim() === "") {
    $(".text-editor .new-block").trigger("click");
}

function serialize() {
    $content = $(".text-editor>div")
        .find(".content")
        .clone();
    $li_clear = $content.find("li");
    $length = $li_clear.length;
    for($i = 0; $i < $length; $i++) {
        $($li_clear[$i]).html($($li_clear[$i])
            .children("span[contenteditable='true']").html());
        $($li_clear[$i]).wrapInner("<div></div>");
    }
    $length = $content.length;
    $result_content = $("<div />");
    $new_block = "";
    for ($i = 0; $i < $length; $i++) {
        if($($content[$i]).prop("tagName").toLowerCase() === "h1") {
            if($i > 0) {
                $result_content.append($new_block);
            }
            $new_block = $("<div />");
        }
        $new_block
            .append($($content[$i])
                .removeAttr("class")
                .removeAttr("contenteditable"));
        if($i === $length-1) {
            $result_content.append($new_block);
        }
    }
    return $result_content.html().replace(/\s+/g, " ");
}