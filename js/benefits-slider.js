if($(".bs-slide").length > 0) {
    $($(".bs-slide")[0]).addClass("active");

    $("#bs-left-arrow").click(function () {
        $active_index = $(".bs-slide").index($(".active"));
        $next_index = $active_index === 0 ?
            $(".bs-slide").length - 1 :
            $active_index - 1;
        benefits_next_slide($next_index);
    });

    $("#bs-right-arrow").click(function () {
        $active_index = $(".bs-slide").index($(".active"));
        $next_index = $active_index === $(".bs-slide").length - 1 ?
            0 :
            $active_index + 1;
        benefits_next_slide($next_index);
    });

    function benefits_next_slide($next_index) {
        $time = 600;
        $(".bs-slide.active")
            .css("transform", "rotateY(90deg)");
        setTimeout(function () {
            $(".bs-slide.active")
                .removeClass("active")
                .css("transform", "rotateY(0)");

            $($(".bs-slide")[$next_index])
                .css("transform", "rotateY(-90deg)")
                .addClass("active")
            setTimeout(function () {
                $($(".bs-slide")[$next_index])
                    .css("transform", "rotateY(0)");
            }, $time);
        }, $time);
    }
}