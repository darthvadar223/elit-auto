$("a.anchor").on("click", function (event) {
    event.preventDefault();
    let id  = $(this).attr('href');
    let top = $(id).offset().top + 1;
    $('body,html').animate({scrollTop: top}, 1250);
});
