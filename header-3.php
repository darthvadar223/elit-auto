<header>
    <div class="wrapper">
        <div class="header-top">
            <div id="ht-left">
                <?php
                include "logo-dark.php";
                include "navigation.php";
                ?>
            </div>
            <div id="ht-right">
                <p><span>+380 (98) 495-35-95</span></p>
                <p>проспект Никопольский, 24</p>
                <p>Мариуполь</p>
            </div>
        </div>
        <div id="header-3-bottom">
            <div>
                <div>
                    <h1>Элит<span>авто</span></h1>
                    <img src="img/waves.svg" alt="волны">
                </div>
                <h2>Прежде всего люди, сплоченные одной целью. Оказываем широкий спектр автоуслуг в Муриполе.</h2>
                <p>Мы предоставляем только надежные, качественные
                    и современные услуги, оставаясь открытыми
                    и дружелюбными  по отношению к клиенту, потому что
                    это делает людей счастливыми.</p>
                <p>Стратегическая цель ЭЛИТАВТО - лидерство в сегменте автосервисов г. Мариуполя.</p>
            </div>
            <img src="img/about/car.png" alt="машина">
        </div>
    </div>
</header>