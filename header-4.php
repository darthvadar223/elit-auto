<header>
    <div class="wrapper">
        <div class="header-top">
            <div id="ht-left">
                <?php
                include "logo-dark.php";
                include "navigation.php";
                ?>
            </div>
            <div id="ht-right">
                <p><span>+380 (98) 495-35-95</span></p>
                <p>проспект Никопольский, 24</p>
                <p>Мариуполь</p>
            </div>
        </div>
    </div>
</header>