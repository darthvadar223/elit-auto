<?php

$data["h1"] = "Кастомизация";
$data["h2"] = "Часто задаваемые вопросы";

$questions = "";

$questions_data = json_decode(file_get_contents("../data/questions.json"), true);//glob('../img/awards-slider/*.{jpg}', GLOB_BRACE);
$count = count($questions_data);

if($count > 0) {
    for ($i = 0; $i < $count; $i++) {
        $header = $questions_data[$i]["header"];
        $content = $questions_data[$i]["content"];
        $incr = $i + 1;
        $questions .= "
<div class='question'>
        <input type='number' min='1' max='99' value='$incr' disabled>
        <div class='center'>
            <div class='content left'>$header</div>
            <div class='vr'></div>
            <div class='content right'>$content</div>
        </div>
        <div class='right'>
            <a name='main' class='edit'>
                <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>
            </a>
            <a name='main' class='remove'>
                <i class='fa fa-times' aria-hidden='true'></i>
            </a>
        </div>
</div>";
    }
} else {
    $questions = "Тут пусто...";
}

$content = "
<div class='long'>
<h1>Часто задаваемые вопросы</h1>
<div class='questions'>
    <form id='add-new'>
        <input type='number' min='1' max='99' value='1'>
        <div class='center'>
            <div class='content left' contenteditable='true' placeholder='Вопрос'></div>
            <div class='vr'></div>
            <div class='content right' contenteditable='true' placeholder='Ответ'></div>
        </div>
        <div class='right add'>
            <a name='main' id='add'>
                <i class='fa fa-plus' aria-hidden='true'></i>
            </a>
            <a name='main' id='ready'>
                <i class='fa fa-check' aria-hidden='true'></i>
                <input type='number'>
            </a>
        </div>
    </form>
    <hr>
    $questions
</div>
</div>";

$script = '
<script>
$(".content").bind("input", function () {
    $caret = $(this).caret();
    $(this).text($(this).text());
    $(this).caret($caret);
});
$("#add").click(function() {
    $order = $(this).parent().parent().children("input").val();
    $header = $(this).parent().parent().children(".center").children(".left").text();
    $content = $(this).parent().parent().children(".center").children(".right").text();
    if($header.trim() !== "" && $content.trim() !== "") {
        $ajaxdata = "order="+$order+"&header="+$header+"&content="+$content;
        $.ajax({
        url: "../ajax/add-question.php",
        method: "post",
        data: $ajaxdata,
        success: function(e) {
            console.log(e);
          $("a[name=\'questions\']").trigger("click");
        },
        error: function() {
          alert("Произошла ошибка, повторите попытку позже");
        }
        });
    } else {
        alert("Для добавления нового вопроса заполните все поля.");
    }
});
$(".remove").click(function() {
    if(confirm("Вы уверены что хотите удалить этот вопрос?")) {
        $index = $(this).parent().parent().children("input").val();
        $ajaxdata = "index="+($index-1);
        $.ajax({
        url: "../ajax/remove-question.php",
        method: "post",
        data: $ajaxdata,
        success: function(e) {
          $("a[name=\'questions\']").trigger("click");
        },
        error: function() {
          alert("Произошла ошибка, повторите попытку позже");
        }
        });
    }
});
$(".edit").click(function() {
    $("#add-new>input").val($(this).parent().parent().children("input").val());
    $("#add-new>.center>.left").text($(this).parent().parent().children(".center").children(".left").text());
    $("#add-new>.center>.right").text($(this).parent().parent().children(".center").children(".right").text());
    $("#add-new>.right").removeClass("add").addClass("edit");
    $("#add-new>.right>#ready>input").val($(this).parent().parent().children("input").val());
});
$("#ready").click(function() {
    $order = $(this).parent().parent().children("input").val();
    $header = $(this).parent().parent().children(".center").children(".left").text();
    $content = $(this).parent().parent().children(".center").children(".right").text();
    $replace = $(this).parent().children("#ready").children("input").val();
    if($header.trim() !== "" && $content.trim() !== "") {
        $ajaxdata = "order="+$order+"&header="+$header+"&content="+$content+"&replace="+$replace;
        $.ajax({
        url: "../ajax/edit-question.php",
        method: "post",
        data: $ajaxdata,
        success: function(e) {
            $("#add-new>.right").removeClass("edit").addClass("add");
            $("a[name=\'questions\']").trigger("click");
        },
        error: function() {
            alert("Произошла ошибка, повторите попытку позже");
        }
        });
    } else {
        alert("Для редактирования вопроса необходимо заполнить все поля.");
    }
});
</script>
';

$content .= $script;



$data["content"] = $content;