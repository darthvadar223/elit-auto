<?php

$data["h1"] = "Отзывы";
$data["h2"] = "";

$bens = "";
$database_config =
    json_decode(
        file_get_contents('../config/database.json'), true);

if ($db = new PDO("mysql:dbname=".$database_config["dbname"].
    ";host=".$database_config["host"],
    $database_config["username"],
    $database_config["password"])) {

    $query = $db->prepare("SELECT * FROM `benefits` ORDER BY `date` DESC");
    $query->execute();
    $result = $query->fetchAll();

    foreach ($result as $row) {
        $photo = $row["photo"] === null ? "-" : "<img src='../img/avatars/".$row["photo"]."'>";
        $class = $row["new"] === '1' ? "class='new'" : "";
        $bens .= "
        <li $class>
            <div>$photo</div>
            <div>".$row["name"]."</div>
            <div>".$row["header"]."</div>
            <div>".($row["estimate"]+1)."</div>
            <div>".$row["benefit"]."</div>
            <div>".date_format(date_create($row["date"]),"d-m-Y H:i:s")."</div>
            <div><input class='check' type='checkbox' ".($row["visible"]?"checked":"")."></div>
            <input style='display: none;' type='number' value='".$row["id"]."'>
        </li>
        ";
    }
}

$content = "
<div class='long'>
<h1>Отзывы</h1>
<a href=''></a>
<ul class='benefits'>
    <li id='header'>
        <div>Фото</div>
        <div>Имя</div>
        <div>Заголовок</div>
        <div>Оценка</div>
        <div>Отзыв</div>
        <div>Дата</div>
        <div>Опубликовано</div>
    </li>
    $bens
</ul>
</div>
";

$script = '
<script>
function check_new() {
    $.ajax({
    url: "../ajax/check-new-benefits.php",
    type: "post"
    });
}
check_new();
check_options();
$(".check").change(function() {
    check_options();
    $.ajax({
    url: "../ajax/benefits-checked.php",
    type: "post",
    data: "id="+$(this).parent().parent().children(\'input\').val()+"&visible="+($(this)[0].checked),
    success: function(e) {
        $("a[name=\'benefits\']").trigger("click");
    }
    });
});
</script>
';

$content .= $script;

$data["content"] = $content;