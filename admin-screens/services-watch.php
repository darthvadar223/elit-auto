<?php

$data["h1"] = "Услуги";
$data["h2"] = "Просмотреть";

//<div class='options'>
//            <select class='type'>
//                <option value='paragraph'>Абзац</option>
//                <option value='title'>Заголовок</option>
//                <option value='subtitle'>Подзаголовок</option>
//            </select>
//            <a class='bold' title='Жирный шрифт'><i class='fa fa-bold' aria-hidden='true'></i></a>
//            <a class='italic' title='Курсивный шрифт'><i class='fa fa-italic' aria-hidden='true'></i></a>
//            <a class='list' title='Список'><i class='fa fa-list-ul' aria-hidden='true'></i></a>
//            <a class='link' title='Вставить ссылку'><i class='fa fa-link' aria-hidden='true'></i></a>
//        </div>
//        <div class='content' contenteditable='true'></div>

$content = "
<div class='long'>
<div id='services'>
<div class='left'>
    <h1>Контент на странице \"Услуги\"</h1>
    <div class='options'>
        <a class='bold' title='Жирный шрифт'><i class='fa fa-bold' aria-hidden='true'></i></a>
        <a class='italic' title='Курсивный шрифт'><i class='fa fa-italic' aria-hidden='true'></i></a>
        <a class='link' title='Вставить ссылку'><i class='fa fa-link' aria-hidden='true'></i></a>
    </div>
    <div class='text-editor'>
        <div></div>
        <ul class='options'>
            <li>
                <button class='new-block'>Новый блок</button>
                <button class='new-form-block'>Блок с формой</button>
            </li>
        </ul>
    </div>
</div>
<div class='right'>
<h1>Услуги</h1>
<ul>
    <li>
        <a>Техническое обслуживание автомобиля</a>
    </li>
    <li>
        <a>Диагностика автомобиля</a>
    </li>
    <li>
        <a>Обслуживание систем кондиционирования и отопления</a>
    </li>
</ul>
</div>
</div>
</div>
";

$script = "";

$script .= "<script>".file_get_contents("../js/jquery-ui.js")."</script>";
$script .= "<script>".file_get_contents("../js/editor.js")."</script>";
$script .= "<script>".file_get_contents("../js/content.js")."</script>";

$content .= $script;

$data["content"] = $content;