<?php

$data["h1"] = "Кастомизация";
$data["h2"] = "Слайдеры";

$main_slider = json_decode(file_get_contents("../img/main-slider/order.json"), true);
$awards_slider = json_decode(file_get_contents("../img/awards-slider/order.json"), true);//glob('../img/awards-slider/*.{jpg}', GLOB_BRACE);

$count = count($main_slider);

$images = "";

for($i = 0; $i < $count; $i++) {
    $name = "../img/main-slider/".$main_slider[$i];
    $images .= "
<div>
    <i id='main' class=\"fa fa-times remove-image\" aria-hidden=\"true\"></i>
    <img src='$name' alt='слайд'>
    <div>
        <div class='left-icons'>
            <a name='main' class='double-left'>
                <i class=\"fa fa-caret-left\" aria-hidden=\"true\"></i>
                <i class=\"fa fa-caret-left\" aria-hidden=\"true\"></i>
            </a>
            <a name='main' class='single-left'>
                <i class=\"fa fa-caret-left\" aria-hidden=\"true\"></i>
            </a>
        </div>
        <input name='main' type='number' min='1' max='99' value='".($i+1)."'>
        <div class='right-icons'>
            <a name='main' class='single-right'>
                <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>
            </a>
            <a name='main' class='double-right'>
                <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>
                <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>
            </a>
        </div>
    </div>
</div>";
}

$wraps_count = $count > 4 ? $count % 4 : 4 - $count;

$wraps = "";

for($i = 0; $i < $wraps_count; $i++) {
    $wraps .= "<div class='wrap'></div>";
}

$content = "<div class='long'><h1>Главный слайдер</h1>
<div class='images'>
$images
$wraps
</div>
<form id='main-form' method='post'>
<label>
Добавить картинку (jpg)
<input type='file' name='photo' accept=\".jpg\">
</label>
</form>
</div>";

$count = count($awards_slider);

$images = "";

for($i = 0; $i < $count; $i++) {
    $name = "../img/awards-slider/".$awards_slider[$i];
    $images .= "
<div>
    <i id='awards' class=\"fa fa-times remove-image\" aria-hidden=\"true\"></i>
    <img src='$name' alt='слайд'>
    <div>
        <div class='left-icons'>
            <a name='awards' class='double-left'>
                <i class=\"fa fa-caret-left\" aria-hidden=\"true\"></i>
                <i class=\"fa fa-caret-left\" aria-hidden=\"true\"></i>
            </a>
            <a name='awards' class='single-left'>
                <i class=\"fa fa-caret-left\" aria-hidden=\"true\"></i>
            </a>
        </div>
        <input name='awards' type='number' min='1' max='99' value='".($i+1)."'>
        <div class='right-icons'>
            <a name='awards' class='single-right'>
                <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>
            </a>
            <a name='awards' class='double-right'>
                <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>
                <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>
            </a>
        </div>
    </div>
</div>";
}

$wraps_count = $count > 4 ? $count % 4 : 4 - $count;

$wraps = "";

for($i = 0; $i < $wraps_count; $i++) {
    $wraps .= "<div class='wrap'></div>";
}

$content .= "<div class='long'><h1>Слайдер с наградами</h1>
<div class='images'>
$images
$wraps
</div>
<form id='awards-form' method='post'>
<label>
Добавить картинку (jpg)
<input type='file' name='photo' accept=\".jpg\">
</label>
</form>
</div>";

$script = '
<script>
$(".double-left").click(function() {
  $index = $(this).parent().parent().parent().index();
    movePhoto($(this).attr("name"), 
    $index, 1);
});
$(".double-right").click(function() {
    $index = $(this).parent().parent().parent().index();
    $length = $(this).parent().parent().parent().parent().children("div").length;
    movePhoto($(this).attr("name"), 
    $index, $length);
});
$(".single-left").click(function() {
  $index = $(this).parent().parent().parent().index();
    movePhoto($(this).attr("name"), 
    $index, $index);
});
$(".single-right").click(function() {
    $index = $(this).parent().parent().parent().index();
    movePhoto($(this).attr("name"), 
    $index, $index+2);
});
$("input[type=\'file\']").change(function() {
    $(this).parent().parent().trigger("submit");
});
$(\'#awards-form\').submit(function(e) {
    e.preventDefault();
    $formdata = new FormData($(this).get(0));
    $formdata.append("directory", "awards");
    $.ajax({
        url: "ajax/add-photo.php",
        type: "POST",
        cache: false,
        contentType: false,
        processData: false,
        data: $formdata,
        success: function (data) {
            $(\'a[name="sliders"]\').trigger("click");
        },
        error: function (data) {
            alert("Произошла ошибка, повторите попытку позже");
        },
    }); 
});
    
$(\'#main-form\').submit(function(e) {
e.preventDefault();
$formdata = new FormData($(this).get(0));
$formdata.append("directory", "main");
$.ajax({
    url: "ajax/add-photo.php",
    type: "POST",
    cache: false,
    contentType: false,
    processData: false,
    data: $formdata,
    success: function (data) {
        $(\'a[name="sliders"]\').trigger("click");
    },
    error: function (data) {
        alert("Произошла ошибка, повторите попытку позже");
    },
}); 
});
    
$("input[type=\'number\']").keypress(function(evt) {
    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
    {
        evt.preventDefault();
    }
});
    
$("input[type=\'number\']").change(function(evt) { 
    if(!isNaN($(this).val())) {
        movePhoto($(this).attr("name"), $(this).parent().parent().index(), $(this).val());
    }
});

function movePhoto($directory, $index, $new_index) {
    $.ajax({
        url: "ajax/move-photo.php",
        type: "post",
        data: "directory="+$directory+"&index="+$index+"&new_index="+$new_index,
        success: function(data) {
          $(\'a[name="sliders"]\').trigger("click");
        },
        error: function (data) {
            alert("Произошла ошибка, повторите попытку позже");
        },
    });
}
    
$(".remove-image").click(function() {
  if(confirm("Вы уверены что хотите удалить этот слайд?")) {
      $index = $(this).parent().index();
      $directory = $(this).attr("id");
      $.ajax({
        url: "ajax/remove-photo.php",
        type: "post",
        data: "directory="+$directory+"&index="+$index,
        success: function(data) {
          $(\'a[name="sliders"]\').trigger("click");
        },
        error: function (data) {
            alert("Произошла ошибка, повторите попытку позже");
        },
      });
  }
});
</script>
';

$content .= $script;

$data["content"] = $content;

