<?php

$data["h1"] = "Заявки";
$data["h2"] = "";

$apps = "";
$database_config =
    json_decode(
        file_get_contents('../config/database.json'), true);

if ($db = new PDO("mysql:dbname=".$database_config["dbname"].
    ";host=".$database_config["host"],
    $database_config["username"],
    $database_config["password"])) {

    $query = $db->prepare("SELECT * FROM `applications` ORDER BY `checked` ASC, `date` DESC");
    $query->execute();
    $result = $query->fetchAll();

    foreach ($result as $row) {
        $apps .= "
        <li>
            <div>".($row["name"]===""?"-":$row["name"])."</div>
            <div>".$row["phone"]."</div>
            <div>".date_format(date_create($row["date"]),"d-m-Y H:i:s")."</div>
            <div><input class='check' type='checkbox' ".($row["checked"]?"checked":"")."></div>
            <input style='display: none;' type='number' value='".$row["id"]."'>
        </li>
        ";
    }
}

$content = "
<div class='long'>
<h1>Заявки</h1>
<a id='read-all'>Отметить все как прочитанное</a>
<ul class='applications'>
    <li id='header'>
        <div>Имя</div>
        <div>Телефон</div>
        <div>Дата</div>
        <div>Прочитано</div>
    </li>
    $apps
</ul>
</div>
";

$script = '
<script>
check_options();
$(".check").change(function() {
    check_options();
    $.ajax({
    url: "../ajax/application-checked.php",
    type: "post",
    data: "id="+$(this).parent().parent().children(\'input\').val()+"&checked="+($(this)[0].checked),
    success: function(e) {
        $("a[name=\'applications\']").trigger("click");
    }
    });
});
$("#read-all").click(function() {
  $.ajax({
    url: "../ajax/read-all.php",
    type: "post",
    success: function() {
        $("a[name=\'applications\']").trigger("click");
    }
  });
});
</script>
';

$content .= $script;

$data["content"] = $content;