<?php

$data["h1"] = "Кастомизация";
$data["h2"] = "Марки автомобилей";

$content = "";

$options = "";
$photos = glob("../img/marks/*.jpg");
$marks_data = json_decode(file_get_contents("../img/marks/marks.json"), true);

$count = count($photos);

for($i = 0; $i < $count; $i++) {
    $datacount = count($marks_data);
    $basename = basename($photos[$i], ".jpg");
    $isnew = true;
    for($j = 0; $j < $datacount; $j++) {
        if($marks_data[$j]["photo"] === $basename) {
            $isnew = false;
            break;
        }
    }
    if($isnew) {
        $photo = $basename;
        $mark_name = ucwords(str_replace("_", " ", $basename));
        $marks_data[] = ["photo"=>$photo,"mark-name"=>$mark_name,"selected"=>false];
    }
}

$count = count($marks_data);
for($i = 0; $i < $count; $i++) {
    if(!file_exists("../img/marks/".$marks_data[$i]["photo"].".jpg")) {
        unset($marks_data[$i]);
        continue;
    }
    $selected = $marks_data[$i]['selected']?'disabled':'';
    $options .=
        "<option value='".$i."' ".$selected.">".$marks_data[$i]["mark-name"]."</option>";
}


asort($marks_data);
$marks_data = array_values($marks_data);
file_put_contents("../img/marks/marks.json", json_encode($marks_data));


$marks = "
<li id='add-new-mark'>
    <form id='add-new-mark-form'>
        <select name='mark' id='mark'>
        $options
        </select>
        <input id='add-mark' type='submit' value='Добавить марку'>
    </form>
</li>
";
$i = 0;
foreach($marks_data as $mark) {
    if(!$mark["selected"]) { $i++; continue; }
    $marks .= "
    <li>
        <i class=\"fa fa-times remove-mark\" aria-hidden=\"true\"></i>
        <input type='number' value='".($i++)."'>
        <img src='../img/marks/".$mark["photo"].".jpg' alt='".$mark["mark-name"]."'>
        <span>".$mark["mark-name"]."</span>
    </li>
    ";
}

$for_wraps = 3;

for($i = 0; $i < $for_wraps; $i++) {
    $marks .= "
    <li class='wrap'></li>
    ";
}

$content .= "
<div class='long'>
<h1>Марки</h1>
<ul class='marks'>
$marks
</ul>
</div>
";

$script = '
<script>
$("#add-new-mark-form").submit(function(e) {
      e.preventDefault();
      $data = $(this).serialize();
      $.ajax({
      url: "../ajax/add-mark.php",
      type: "post",
      data: $data,
      success: function(e) {
        $("a[name=\'marks\']").trigger("click");
      }
      });
});
$(".remove-mark").click(function() {
    $index = $(this).siblings("input").val();
    $.ajax({
        url: "../ajax/remove-mark.php",
        type: "post",
        data: "remove="+$index,
        success: function(e) {
            $("a[name=\'marks\']").trigger("click");
        },
    });
});
</script>
';

$content .= $script;

$data["content"] = $content;