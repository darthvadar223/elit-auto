<?php
include "head.php";
$include = "";
if(!isset($_GET["id"])) {
    $include = "services-section.php";
}
elseif(isset($_GET["id"]) && !isset($_GET["label"])) {
    $h1 = "Диагностика автомобиля";
    $include = "services-single.php";
} else {
    $h1 = "Комплексная диагностика автомобиля";
    $include = "services-single-label.php";
}
include "header-2.php";
?>
    <article>
        <?php
        include "$include";
        include "map.php";
        ?>
    </article>
<?php
include "footer.php";
?>

