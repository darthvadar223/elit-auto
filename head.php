<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?=$title." — ".$title_after?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="favicon.png">
    <link rel="stylesheet" type="text/css" href="css/<?=$page?>/<?=$page?>.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,500,700" rel="stylesheet">
</head>
<body class="<?=$page?>">
<?php
if(isset($_SESSION["logged_in"])) {
    echo "<a id='admin-panel' href='admin'>admin</a>";
}
?>