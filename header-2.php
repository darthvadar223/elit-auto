<header>
    <div class="wrapper">
        <div class="header-top">
            <div id="ht-left">
                <?php
                include "logo.php";
                include "navigation.php";
                ?>
            </div>
            <div id="ht-right">
                <p><span>+380 (98) 495-35-95</span></p>
                <p>проспект Никопольский, 24</p>
                <p>Мариуполь</p>
            </div>
        </div>
        <?php if(isset($h1)) echo "<h1>" . $h1 . "</h1>" ?>
    </div>
</header>