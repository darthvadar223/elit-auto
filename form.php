<div class="shadow">
    <div class="form">
        <img src="img/icon-close.svg" alt="закрыть">
        <div class="form-default">
            <div class="top-default">
                <img class="dot" src="img/dot.svg" alt="точка">
                <img src="img/form/mail.svg" alt="механик">
            </div>
            <h1 class="default">Окажем консультацию оставьте заявку</h1>
            <form class="main-form" method="post">
                <div>
                    <label>Имя</label>
                    <input name="name" autocomplete="off" type="text" placeholder="Ваше имя">
                </div>
                <div>
                    <label>Телефон *</label>
                    <input name="phone" autocomplete="off" type="tel" placeholder="0(00)-000-00-00">
                    <span>Укажите свой номер телефона</span>
                </div>
                <input type="submit" value="Оставить заявку">
            </form>
        </div>
    </div>
</div>