<?php
session_start();

if(isset($_POST["checked"])
    && is_numeric($_POST["id"])
    && isset($_SESSION["logged_in"])) {
    $database_config =
        json_decode(
            file_get_contents('../config/database.json'), true);
    if ($db = new PDO("mysql:dbname=".$database_config["dbname"].
        ";host=".$database_config["host"],
        $database_config["username"],
        $database_config["password"])) {

        $checked = $_POST["checked"] === "true" ? 1 : 0;
        $db->query(
            "UPDATE `applications` 
            SET `checked` = '".$checked."' 
            WHERE `applications`.`id` = ".$_POST["id"]);

    }
}