<?php
session_start();
if(is_numeric($_POST["order"])
    && isset($_POST["header"])
    && isset($_POST["content"])
    && isset($_POST["replace"])
    && isset($_SESSION["logged_in"])) {
    $data = json_decode(file_get_contents("../data/questions.json"), true);

    unset($data[$_POST["replace"]-1]);
    array_splice( $data, $_POST["order"] - 1, 0,
    array([
        "header" => $_POST["header"],
        "content" => $_POST["content"]
    ]));
    $data = array_values($data);

    file_put_contents("../data/questions.json", json_encode($data));
}