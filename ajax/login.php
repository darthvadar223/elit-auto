<?php
session_start();

if(isset($_POST["name"])
&& isset($_POST["password"])) {
    if($_POST["name"] === "admin") {
        $data = json_decode(file_get_contents("../data/main_admin.json"), true);
        if(password_verify($_POST["password"], $data["password"])) {
            $_SESSION["logged_in"] = "admin";
        }
    }
}