<?php
session_start();

if(isset($_SESSION["logged_in"])) {
    $database_config =
        json_decode(
            file_get_contents('../config/database.json'), true);

    if ($db = new PDO("mysql:dbname=".$database_config["dbname"].
        ";host=".$database_config["host"],
        $database_config["username"],
        $database_config["password"])) {
        $db->query("UPDATE `benefits`
                        SET `new` = '0'");
    }
}
