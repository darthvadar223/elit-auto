<?php
session_start();
if(is_numeric($_POST["remove"]) && isset($_SESSION["logged_in"])) {
    $data = json_decode(file_get_contents("../img/marks/marks.json"), true);
    $data[$_POST["remove"]]["selected"] = false;
    file_put_contents("../img/marks/marks.json", json_encode($data));
}