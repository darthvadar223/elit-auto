<?php
session_start();
if(is_numeric($_POST["index"])
    && isset($_SESSION["logged_in"])) {
    $data = json_decode(file_get_contents("../data/questions.json"), true);

    unset($data[$_POST["index"]]);
    $data = array_values($data);

    file_put_contents("../data/questions.json", json_encode($data));
}