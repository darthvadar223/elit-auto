<?php
session_start();

if(is_numeric($_POST["index"])
    && isset($_POST["directory"])
    && is_numeric($_POST["new_index"])
    && isset($_SESSION["logged_in"])) {
    $index = $_POST["index"];
    $new_index = $_POST["new_index"] - 1;
    switch ($_POST["directory"]) {
        case "awards":
            $data = json_decode(file_get_contents("../img/awards-slider/order.json"), true);
            if($data[$new_index]) {
                $new = $data[$index];
                unset($data[$index]);
                array_splice( $data, $new_index, 0, $new );
                $data = array_values($data);
            } else {
                $data[count($data)] = $data[$index];
                unset($data[$index]);
                $data = array_values($data);
            }
            file_put_contents("../img/awards-slider/order.json", json_encode($data));
            break;
        case "main":
            $data = json_decode(file_get_contents("../img/main-slider/order.json"), true);
            if($data[$new_index]) {
                $new = $data[$index];
                unset($data[$index]);
                array_splice( $data, $new_index, 0, $new );
                $data = array_values($data);
            } else {
                $data[count($data)] = $data[$index];
                unset($data[$index]);
                $data = array_values($data);
            }
            file_put_contents("../img/main-slider/order.json", json_encode($data));
            break;
    }
}