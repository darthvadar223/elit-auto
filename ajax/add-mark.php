<?php
session_start();

if(is_numeric($_POST["mark"]) && isset($_SESSION["logged_in"])) {
    $data = json_decode(file_get_contents("../img/marks/marks.json"), true);
    $data[$_POST["mark"]]["selected"] = true;
    file_put_contents("../img/marks/marks.json", json_encode($data));
}