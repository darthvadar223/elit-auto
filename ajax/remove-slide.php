<?php
session_start();
if(is_numeric($_POST["index"]) && isset($_POST["directory"])
    && isset($_SESSION["logged_in"])) {
    $index = $_POST["index"];
    switch ($_POST["directory"]) {
        case "awards":
            $data = json_decode(file_get_contents("../img/awards-slider/order.json"), true);
            unlink("../img/awards-slider/$data[$index]");
            unset($data[$index]);
            $data = array_values($data);
            file_put_contents("../img/awards-slider/order.json", json_encode($data));
            break;
        case "main":
            $data = json_decode(file_get_contents("../img/main-slider/order.json"), true);
            unlink("../img/main-slider/$data[$index]");
            unset($data[$index]);
            $data = array_values($data);
            file_put_contents("../img/main-slider/order.json", json_encode($data));
            break;
    }
}