<?php

if(isset( $_POST["name"] )
    && isset( $_POST["header"] )
    && isset( $_POST["estimate"] )
    && isset( $_POST["benefit"] )) {

    $database_config =
        json_decode(
            file_get_contents('../config/database.json'), true);

    if ($db = new PDO("mysql:dbname=".$database_config["dbname"].
        ";host=".$database_config["host"],
        $database_config["username"],
        $database_config["password"])) {
        $photo_name = "NULL";
        if ($_FILES["photo"]["error"] === 0) {
            $uploaddir = '../img/avatars/';
            $rand = rand(100000000, 999999999);
            $i = 0;
            while($db->query("SELECT COUNT(`photo`) FROM `benefits` WHERE `photo` = '".$rand.".jpg'") === 0) {
                $rand = rand(100000000, 999999999);
                if($i > 100) { header('HTTP/1.0 400 Bad error'); }
                $i++;
            }
            $new_name = $rand.".jpg";
            $uploadfile = $uploaddir . $new_name;

            $maxDim = 300;
            $file_name = $_FILES['photo']['tmp_name'];
            list($width, $height, $type, $attr) = getimagesize( $file_name );
            if ( $width > $maxDim || $height > $maxDim ) {
                $target_filename = $file_name;
                $ratio = $width/$height;
                if( $ratio > 1) {
                    $new_width = $maxDim;
                    $new_height = $maxDim/$ratio;
                } else {
                    $new_width = $maxDim*$ratio;
                    $new_height = $maxDim;
                }
                $src = imagecreatefromstring( file_get_contents( $file_name ) );
                $dst = imagecreatetruecolor( $new_width, $new_height );
                imagecopyresampled( $dst, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
                imagedestroy( $src );
                imagejpeg( $dst, $target_filename ); // adjust format as needed
                imagedestroy( $dst );

                move_uploaded_file($target_filename, $uploadfile);
                $photo_name = "'" . $new_name . "'";
            }
        }

        $db->query("
    INSERT INTO `benefits` (`id`, `name`, `header`, `estimate`, `benefit`, `photo`, `date`, `visible`)
    VALUES (NULL, '".$_POST["name"]."',
    '".$_POST["header"]."',
    '".$_POST["estimate"]."',
    '".$_POST["benefit"]."',
    ".$photo_name.",
    CURRENT_TIMESTAMP,
    '0');");

    }
} else {
    header('HTTP/1.0 400 Bad error');
}