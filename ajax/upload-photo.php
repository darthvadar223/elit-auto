<?php
session_start();
if(isset($_FILES['photo'])
    && isset($_SESSION["logged_in"])) {

    $uploaddir = '../img/photos/';

    $rand = rand(100000000, 999999999);
    $i = 0;
    while(file_exists($uploaddir."$rand.jpg")) {
        $rand = rand(100000000, 999999999);
        if($i > 100) { header('HTTP/1.0 400 Bad error'); }
        $i++;
    }

    $_FILES['photo']['name'] = $rand.".jpg";
    $data = json_decode(file_get_contents("../img/awards-slider/order.json"), true);
    $data[] = $_FILES['photo']['name'];
    file_put_contents("../img/awards-slider/order.json", json_encode($data));
    $uploadfile = $uploaddir . basename($_FILES['photo']['name']);
    move_uploaded_file($_FILES['photo']['tmp_name'], $uploadfile);

}