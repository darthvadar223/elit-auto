<?php
session_start();
if(isset($_SESSION["logged_in"])) {
    $database_config =
        json_decode(
            file_get_contents('../config/database.json'), true);
    if ($db = new PDO("mysql:dbname=" . $database_config["dbname"] .
        ";host=" . $database_config["host"],
        $database_config["username"],
        $database_config["password"])) {

        $data = [
            "application_count" => null,
            "benefit_count" => null
        ];

        $query = $db->prepare("SELECT COUNT(`checked`) as `count` FROM `applications` WHERE `checked` = 0");
        $query->execute();
        $result = $query->fetchAll();

        $data["application_count"] = $result[0]["count"];

        $query = $db->prepare("SELECT COUNT(`new`) as `count` FROM `benefits` WHERE `new` = 1");
        $query->execute();
        $result = $query->fetchAll();

        $data["benefit_count"] = $result[0]["count"];

        echo json_encode($data);
    }
}