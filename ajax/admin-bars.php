<?php
session_start();

if(isset($_POST["name"]) && isset($_SESSION["logged_in"])) {
    $data = [
        "h1" => "",
        "h2" => "",
        "content" => ""
    ];

    if (file_exists("../admin-screens/" . $_POST["name"] . ".php")) {
        include "../admin-screens/" . $_POST["name"] . ".php";
    } else {
        $data["h1"] = "404";
        $data["h2"] = "Страница не найдена";
    }

    echo json_encode($data);
}