<section id="services-single-1">
    <div class="wrapper">
        <div>
            <a href="main">Главная</a>
            <svg width="7px" height="7px" viewBox="0 0 15 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Mask" fill="#323656" fill-rule="nonzero"><polygon points="0 21.18 9.27125506 12 0 2.82 2.85425101 0 15 12 2.85425101 24"></polygon></g></g></svg>
            <a href="services">Услуги</a>
            <svg width="7px" height="7px" viewBox="0 0 15 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Mask" fill="#323656" fill-rule="nonzero"><polygon points="0 21.18 9.27125506 12 0 2.82 2.85425101 0 15 12 2.85425101 24"></polygon></g></g></svg>
            <a href="services?id=1">Диагностика автомобиля</a>
            <svg width="7px" height="7px" viewBox="0 0 15 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Mask" fill="#323656" fill-rule="nonzero"><polygon points="0 21.18 9.27125506 12 0 2.82 2.85425101 0 15 12 2.85425101 24"></polygon></g></g></svg>
            <a>Комплексная диагностика автомобиля</a>
        </div>
    </div>
</section>
<section id="services-single-2">
    <div class="wrapper">
        <div class="services-content">
            <div>
                <h1>Что мы можем предложить?</h1>
                <p>Комплексная диагностика автомобиля, осуществляемая нашим техцентром, чаще всего востребована перед покупкой автомобиля с пробегом, ведь ни для кого не секрет, как может омрачить радость покупки внезапный дорогостоящий ремонт.</p>
                <p><span>ЭЛИТАВТО</span> предлагает, во избежание непредвиденных расходов, провести полную проверку всех механических и электрических (электронных) систем автомобиля. Наши специалисты, обладая специальными навыками и многолетним опытом, проведут все необходимые проверочно-диагностические операции и тесты на предмет обнаружения явных и скрытых дефектов подвески, рулевого управления, тормозной системы, силового кузова и каркаса (рамы, лонжеронов, подрамников и прочих), и состояние лакокрасочного покрытия, а также состояние всех заправочных объёмов автомобиля - их наполненность и состояние жидкостей.
                    Мастера диагностического участка проверят функционирование всех блоков, модулей и систем тестируемого, с обязательным осмотром свечей зажигания, воздушного и салонного фильтров.</p>
                <p>По окончании комплексной диагностики клиенту выдается &quot;Ведомость выявленных дефектов&quot;, где мастера указывают все необходимые к устранению неисправности с обязательным комментарием о срочности исполнения, а также стоимость работ и запчастей. Благодаря проведённым диагностическим процедурам, заказчик будет иметь полное представление об автомобиле и сможет реально оценить его состояние, а зачастую и иметь аргументы для торга при приобретении.</p>
                <div class="image">
                    <img src="img/main/test.jpg" alt="картинка">
                    <?php
                    include "logo.php";
                    ?>
                </div>
            </div>
            <div>
                <h1>В каких случаях делают
                    комплексную диагностику автомобиля?</h1>
                <ul>
                    <li><div><b>Планово — раз в полгода.</b><br>
                        Такая комплексная диагностика автомобиля позволяет во время устранять мелкие неприятности, а также предупреждать серьезные поломки, которые могут возникнут в процессе эксплуатации авто. Такая проверка не обязательная, но если Вы хотите ездить на надежном автомобиле, то непременно проходите такую диагностику.</div></li>
                    <li><div><b>Вы решили купить автомобиль, но не уверены в правдивом описании его недостатков продавцом.</b><br>
                        Тут все просто. После проведения комплексной диагностики автомобиля, и получения листа с указанием всех дефектов, с начесным продавцом можно решить проблему на раз.</div></li>
                    <li><div><b>Вы заметили, что у вас что-то не так в машине и пригнали авто в какой-то автосервис.</b><br>
                        Там его осмотрел специалист и сделал заключение, что на Вашем авто нужно сменить кучу всего на свете и отрегулировать все абсолютно, плюс приобрести автозапчасти для замены, при этом стоимость работ исчисляется десятками тысяч.
                        Вам это не понравилось и Вы решили перестраховаться.
                        В этом случае, самое разумное, сделатькомплексную диагностику автомобиля. После нее Вас просто невозможно будет «развести» на деньги.</div></li>
                    <li><div><b>Многие любят путешествовать на своем железном коне, как по стране так и за рубеж.</b><br>
                        Это нормальное явление. Но дорого есть дорого. Чтобы не голосовать на трассе после поломки, перед поездкой, лучше сделать комплексную диагностику авто. Это даст возможность подготовить машину к длительному пробегу и не заниматься ремонтом
                        в дорогое.
                        Ведь дома и стены помогают, а в дороге цена ремонта может
                        возрастать в разы.
                        Виды работ для проведения комплексной диагностики автомобиля.</div></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="services-leave-application">
    <div class="wrapper">
        <h1>Элит Авто в Мариуполе</h1>
        <p>Будьте  уверенным в своей безопасности
            и наслаждайтесь комфортной поездкой
            на исправном автомобиле</p>
        <a class="sign-up">Оставить заявку</a>
    </div>
</section>
<section id="services-single-3">
    <div class="wrapper">
        <div class="services-content">
            <div>
                <h1>Почему диагностику автомобиля нужно
                    доверить специалистам <span>Элитавто</span>?</h1>
                <p><b>Во-первых</b>, целый комплекс неисправностей, появляющихся в ходе эксплуатации автомобиля, под силу диагностировать только специалистам высочайшей квалификации с опытом диагностики даже незначительных «отклонений» от нормы. Частные «гаражные мастера» в большинстве случаев проводят поверхностный осмотр, выявляя неисправность буквально «на глаз», без использования какой-либо электроники. FIT SERVICE укомплектован самым современным компьютерным диагностическим оборудованием, которое в руках опытных мастеров позволяет безошибочно диагностировать все ошибки автомобиля.</p>
                <p><b>Во-вторых</b>, мы вкладываем большие средства, в современное оборудование, аналогов которого практически нет в России. Это значит, что диагностика выполняется с помощью компьютеризованных систем, и исключают «человеческий фактор» при проведении диагностики автомобиля.</p>
                <p><b>В-третьих</b>, После прохождения диагностики специалист автосервиса расскажет Вам о всех неисправностях и предложит варианты решения проблемы. Вы получите на руки подробную диагностическую карту, в которой будут указаны как неисправности, так и рекомендованные ремонтные работы вместе с ценой и необходимыми запчастями.</p>
                <p>Мы обслуживаем легковые авто, кроссоверы, джипы, грузовики и автомобили сложной конструкции. Диагностика японских автомобилей, европейцев, американцев и корейцев, а также народных автомобилей ВАЗ — наша специализация. Мы гарантируем качественную и полную диагностику вашего автомобиля с последующей выдачей развернутых результатов.</p>
            </div>
            <div>
                <h1>5 ситуаций, когда требуется
                    диагностика авто</h1>
                <ul>
                    <li><div><a href="services?id=1&label=1">Диагностика перед покупкой автомобиля;</a></div></li>
                    <li><div>В преддверии весеннего сезона;</div></li>
                    <li><div>В преддверии зимнего сезона;</div></li>
                    <li><div>Перед путешествием на автомобиле;</div></li>
                    <li><div>При наличии признаков неисправностей (автомобиль работает не так как раньше, появились посторонние шумы, скрипы, автомобиль
                        начал «дергаться» или наблюдается потеря мощности).</div></li>
                </ul>
            </div>
            <div>
                <h2>Запишитесь к нам</h2>
                <div class="type-1">
                    <img src="img/main/test.jpg" class="background">
                    <div>
                        <?php
                        include "form.php";
                        include "logo.php";
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>