<footer>
    <div class="wrapper">
        <div>
            <?php
            include "logo.php";
            include "navigation.php";
            ?>
            <div id="work">
                <h1>Режим работы</h1>
                <ul>
                    <li>
                        <span>пн-пт</span>
                        <p>8:00 - 17:00</p>
                    </li>
                    <li>
                        <span>сб</span>
                        <p>8:00 - 12:00</p>
                    </li>
                    <li>
                        <span>вс</span>
                        <p>Выходной</p>
                    </li>
                </ul>
            </div>
            <div id="address">
                <div>
                    <span>+380 (98) 495-35-95</span>
                    <p>проспект Никопольский, 24</p>
                    <p>Мариуполь</p>
                </div>
                <a class="sign-up">Записаться</a>
            </div>
        </div>
        <p>© 2018. «ЭлитАвто». Все права защищены</p>
    </div>
</footer>
<div id="overlay">
    <?php
    include "form.php";
    ?>
</div>
<?php
if($scripts) {
    $count = count($scripts);
    for ($i = 0; $i < $count; $i++) {
        echo "<script src=\"js/$scripts[$i].js\"></script>";
    }
}
?>
</body>
</html>