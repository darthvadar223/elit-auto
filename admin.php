<?php session_start(); ?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Админ панель</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/admin/admin.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">
    <link rel="stylesheet" href="fonts/css/font-awesome.min.css">
</head>
<body>
<?php if(isset($_SESSION["logged_in"])): ?>
<article>
    <a id="menu" class="menu-closed">
        <i class="fa fa-arrow-right" aria-hidden="true"></i>
    </a>
    <aside id="aside-left">
        <a id="return-to-home" href="/">
            <i class="fa fa-home" aria-hidden="true"></i>
            <span>Вернуться на главную</span>
        </a>
        <ul class="notifications">
            <li id="applications">
                <i class="fa fa-address-book" aria-hidden="true"></i>
            </li>
            <li id="benefits">
                <i class="fa fa-comments" aria-hidden="true"></i>
            </li>
            <li id="settings">
                <i class="fa fa-cog" aria-hidden="true"></i>
            </li>
        </ul>
        <ul class="menu">
            <li class="default selected focused">
                <a class="navigate" name="start-page">
                    <div>
                        <i class="fa fa-file-text left" aria-hidden="true"></i>
                        <span>Стартовая страница</span>
                    </div>
                </a>
            </li>
        </ul>
        <h1>Кастомизация</h1>
        <ul class="menu">
            <li class="dropdown">
                <a>
                    <div>
                        <i class="fa fa-paint-brush left" aria-hidden="true"></i>
                        <span>Кастомизация</span>
                        <i class="fa fa-caret-down right" aria-hidden="true"></i>
                    </div>
                </a>
                <ul>
                    <li><a class="navigate" name="sliders">Слайдеры</a></li>
                    <li><a class="navigate" name="questions">Часто задаваемые вопросы</a></li>
                    <li><a class="navigate" name="marks">Марки автомобилей</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a>
                    <div>
                        <i class="fa fa-car left" aria-hidden="true"></i>
                        <span>Услуги</span>
                        <i class="fa fa-caret-down right" aria-hidden="true"></i>
                    </div>
                </a>
                <ul>
                    <li><a class="navigate" name="services-watch">Просмотреть</a></li>
                    <li><a class="navigate" name="services-add-new">Добавить новую</a></li>
                </ul>
            </li>
        </ul>
        <h1>Уведомления</h1>
        <ul class="menu">
            <li class="default">
                <a class="navigate" name="applications">
                    <div>
                        <i class="fa fa-address-book left" aria-hidden="true"></i>
                        <span>Заявки</span>
                    </div>
                </a>
            </li>
            <li class="default">
                <a class="navigate" name="benefits">
                    <div>
                        <i class="fa fa-comments left" aria-hidden="true"></i>
                        <span>Отзывы</span>
                    </div>
                </a>
            </li>
        </ul>
        <h1>Дополнительно</h1>
        <ul class="menu">
            <li class="default">
                <a class="navigate" name="telegram">
                    <div>
                        <i class="fa fa-envelope left" aria-hidden="true"></i>
                        <span>Написать разработчику</span>
                    </div>
                </a>
            </li>
            <li class="default">
                <a class="navigate" name="settings">
                    <div>
                        <i class="fa fa-cog left" aria-hidden="true"></i>
                        <span>Настройки</span>
                    </div>
                </a>
            </li>
        </ul>
    </aside>
    <aside id="aside-right">
        <div class="overlay"><div class="lds-ripple"><div></div><div></div></div></div>
        <div class="top">
            <h1></h1>
            <h2></h2>
        </div>
        <div class="bottom"></div>
    </aside>
</article>
<script src="js/jquery.js"></script>
<script src="js/caret.js"></script>
<script src="js/admin-bar.js"></script>
<script src="js/menu.js"></script>
<?php else: ?>
<article>
    <section id="login">
        <div>
            <div class="top">
                <h1>Вход</h1>
                <form id="login-form" method="post">
                    <input autocomplete="off" name="name" type="text" placeholder="Имя" required>
                    <input name="password" type="password" placeholder="Пароль" required>
                    <button>Войти</button>
                </form>
            </div>
            <div class="bottom">
                <a id="forget-password">Забыли пароль?</a>
            </div>
        </div>
        <a href="/"><- Вернуться на главную</a>
    </section>
</article>
<script src="js/jquery.js"></script>
<script>
$("#login-form").submit(function (e) {
    e.preventDefault();
    $.ajax({
        url: "ajax/login.php",
        type: "post",
        data: $(this).serialize(),
        success: function () {
            location.reload();
        }
    });
});
</script>
<?php endif; ?>
</body>
</html>
