<?php
include "head.php";
include "header-3.php";
?>
    <article>
        <section id="about-1">
            <div class="wrapper">
                <h1>Вы спросите почему мы?</h1>
            </div>
        </section>
        <section id="about-2">
            <div class="wrapper">
                <h1><span>Награды</span> скажут за нас</h1>
                <img src="img/waves.svg" alt="волны">
                <ul>
                    <li>
                        <div>
                            <img class="dot dot-tl" src="img/dot.svg" alt="точка">
                            <img src="img/main/suspension.svg" alt="подвеска"/>
                        </div>
                        <h2>Сервис</h2>
                        <p>Полный набор услуг по обслуживанию и ремонту любого автомобиля</p>
                    </li>
                    <li>
                        <div>
                            <img class="dot dot-tr dot-180" src="img/dot.svg" alt="точка">
                            <img src="img/main/disc-brake.svg" alt="диск">
                        </div>
                        <h2>Качество</h2>
                        <p>Заказываем только проверенные качественные запчасти</p>
                    </li>
                    <li>
                        <div>
                            <img class="dot dot-bl dot-90" src="img/dot.svg" alt="точка">
                            <img src="img/main/mechanic.svg" alt="механик">
                        </div>
                        <h2>Квалификация</h2>
                        <p>Высокая квалификация персонала и многолетний опыт обслуживания</p>
                    </li>
                </ul>
                <div id="slider">
                    <div class="main">
                        <img id="left-arrow" src="img/about/left-arrow.svg" alt="стрелка влево">
                        <div class="images">
                            <div>
                            <?php
                            $data = json_decode(file_get_contents("img/awards-slider/order.json"), true);
                            foreach ($data as $slide):
                                ?>
                                    <img src="img/awards-slider/<?=$slide?>" alt="слайд">
                            <?php
                            endforeach;
                            ?>
                            </div>
                        </div>
                        <img id="right-arrow" src="img/about/right-arrow.svg" alt="стрелка вправо">
                    </div>
                    <div class="dots"></div>
                </div>
            </div>
        </section>
        <section id="about-3">
            <?php
            include "three-services.php";
            ?>
        </section>
        <?php
        include "map.php";
        ?>
    </article>
<?php
include "footer.php";
?>