<section id="services-section">
    <div class="wrapper">
        <h1>Предоставляемые услуги</h1>
        <ul>
            <?php for($i = 0; $i < 12; $i++):
                $svg = "suspension";
                ?>
                <li>
                    <a name="1" href="services?id=1">
                        <div class="main">
                            <?php echo file_get_contents("img/services/svg/$svg.svg"); ?>
                            <h1>Техническое обслуживание автомобиля</h1>
                        </div>
                        <div class="back">
                            <div class="overlay"></div>
                            <img src="img/main/test.jpg" alt="элит авто">
                        </div>
                    </a>
                </li>
            <?php endfor; ?>
        </ul>
    </div>
</section>