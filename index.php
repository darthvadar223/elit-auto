<?php
session_start();
//error_reporting(0);
$database_config =
    json_decode(
        file_get_contents('config/database.json'), true);
$request_uri = "/" . $_GET["path"];
if($request_uri) {
    $page = "";
    $title = "";
    $scripts = [];
    switch ($request_uri) {
        case "/":
        case "/main":
            $title = "Элит Авто";
            $title_after = "Ремонт и техобслуживание автомобиля в Мариуполе";
            $page = "main";
            $scripts = [
                "jquery",
                "jquery.mask.min",
                "anchors",
                "highlight-menu",
                "main-slider",
                "tabs",
                "form",
                "sign-up",
                "services-hover",
                "benefits-slider",
                "empty-wrapper"
            ];
            break;
        case "/services":
            $title = "Услуги";
            $title_after = "Элит Авто";
            $page = "services";
            $scripts = [
                "jquery",
                "jquery.mask.min",
                "highlight-menu",
                "form",
                "sign-up",
                "empty-wrapper"
            ];
            break;
        case "/about":
            $title = "О нас";
            $title_after = "Элит Авто";
            $page = "about";
            $scripts = [
                "jquery",
                "jquery.mask.min",
                "services-hover",
                "highlight-menu",
                "form",
                "sign-up",
                "awards-slider",
                "empty-wrapper"
            ];
            break;
        case "/benefits":
            $title = "Отзывы";
            $title_after = "Элит Авто";
            $page = "benefits";
            $scripts = [
                "jquery",
                "jquery.mask.min",
                "highlight-menu",
                //"autosize",
                "random-color",
                "form",
                "sign-up",
                "leave-benefit",
                "empty-wrapper"
            ];
            break;
        case "/contacts":
            $title = "Контакты";
            $title_after = "Элит Авто";
            $page = "contacts";
            $scripts = [
                "jquery",
                "jquery.mask.min",
                "highlight-menu",
                "form",
                "sign-up",
                "empty-wrapper"
            ];
            break;
        case "/admin":
            include "admin.php";
            break;
        default:
            echo "Страница не найдена<br><a href='/'>Вернуться на главную</a>";
            break;
    }
    if($page !== "")
        include "$page.php";
}
