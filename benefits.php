<?php
include "head.php";
include "header-4.php";
?>
    <div id="benefit-overlay">
        <div>
            <div>
                <img src="img/icon-close.svg" alt="закрыть">
                <h1>Ваш отзыв</h1>
                <form id="benefit-form" method="post">
                    <div class="input">
                        <label>Ваше имя *</label>
                        <input autocomplete="off" name="name" type="text" placeholder="Ваше имя">
                    </div>
                    <div class="input">
                        <label>Заголовок *</label>
                        <input autocomplete="off" name="header" type="text"
                               placeholder="Например, лучший автосервис в Мариуполе">
                    </div>
                    <div class="estimate">
                        <label>Оценка *</label>
                        <input name="estimate" type="number"/>
                        <div class="stars">
                            <img src="img/star-empty.svg" alt="пустая звезда">
                            <img src="img/star-empty.svg" alt="пустая звезда">
                            <img src="img/star-empty.svg" alt="пустая звезда">
                            <img src="img/star-empty.svg" alt="пустая звезда">
                            <img src="img/star-empty.svg" alt="пустая звезда">
                        </div>
                    </div>
                    <div class="textarea">
                        <label>Отзыв *</label>
                        <textarea id="autosize" maxlength="208" autocomplete="off" name="benefit" type="text"
                                  placeholder="Ваш комментарий"></textarea>
                    </div>
                    <label class="file">
                        <img src="img/benefits/clip.svg" alt="скрепка">
                        <input name="photo" type="file" accept=".jpg"/>
                        <div>Прикрепить фото (jpg)</div>
                    </label>
                    <span>Пожалуйста, заполните пустые поля</span>
                    <div class="submit">
                        <input id="submit" class="disabled" type="submit" value="Оставить отзыв"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <article>
        <section id="benefits-1">
            <div class="wrapper">
                <h1>Есть что сказать?</h1>
                <p>Оставьте отзыв, чтобы они грели душу ночами нашим специалистам
                    или мы знали где нам стоит «поднажать», чтобы наверняка мы были лучшими</p>
                <a id="leave-benefit">Оставить отзыв</a>
            </div>
        </section>
        <section id="benefits-2">
            <div class="wrapper">
                <ul>
                    <?php if($db = new PDO(
                            "mysql:dbname=".$database_config["dbname"].
                            ";host=".$database_config["host"],
                            $database_config["username"],
                            $database_config["password"])):
                        $getAll = $db->prepare("SELECT * FROM `benefits` WHERE `visible` = 1 ORDER BY `date` DESC");
                        $getAll->execute();
                        $data = $getAll->fetchAll();
                        foreach ($data as $row):
                    ?>
                    <li>
                        <div class="avatar-container">
                            <div>
                                <?php if($row["photo"] !== null): ?>
                                <img src="img/avatars/<?=$row["photo"]?>" alt="аватарка">
                                <?php else: ?>
                                <span class="random-color"></span>
                                <?php endif;?>
                            </div>
                            <img src="img/benefits/shape.svg" alt="украшение">
                        </div>
                        <div class="benefit-container">
                            <h1>«<?=$row["header"]?>»</h1>
                            <div class="stars">
                                <?php
                                $estimate = (int)$row["estimate"];
                                for($i = 0; $i < 5; $i++) {
                                    if($estimate >= $i) {
                                        echo "<img src=\"img/star.svg\" alt=\"звезда\">";
                                    } else {
                                        echo "<img src=\"img/star-empty.svg\" alt=\"пустая звезда\">";
                                    }
                                }
                                ?>
                            </div>
                            <p><?=$row["benefit"]?></p>
                            <div class="info">
                                <span><?=$row["name"]?></span>
                                <img src="img/wave.svg" alt="волна">
                                <p><?=date("d.m.Y", strtotime($row["date"]))?></p>
                            </div>
                        </div>
                    </li>
                    <?php
                        endforeach;
                        endif; ?>
                </ul>
            </div>
        </section>
        <?php
        include "map.php";
        ?>
    </article>
<?php
include "footer.php";
?>