<?php
include "head.php";
include "header-1.php";
?>
<article>
    <section id="main-1">
        <div class="wrapper">
            <div id="m1-left">
                <h1>Почему «Элит Авто» рулит?</h1>
                <ul>
                    <li>
                        <div>
                            <img class="dot dot-tl" src="img/dot.svg" alt="точка">
                            <img src="img/main/suspension.svg" alt="подвеска"/>
                        </div>
                        <p>У нас полный набор услуг по обслуживанию и ремонту любого автомобиля</p>
                    </li>
                    <li>
                        <div>
                            <img class="dot dot-tr dot-180" src="img/dot.svg" alt="точка">
                            <img src="img/main/disc-brake.svg" alt="диск">
                        </div>
                        <p>Мы заказываем только проверенные качественные запчасти</p>
                    </li>
                    <li>
                        <div>
                            <img class="dot dot-bl dot-90" src="img/dot.svg" alt="точка">
                            <img src="img/main/mechanic.svg" alt="механик">
                        </div>
                        <p>Высокая квалификация персонала и многолетний опыт обслуживания</p>
                    </li>
                    <li>
                        <div>
                            <img class="dot dot-br dot-270" src="img/dot.svg" alt="точка">
                            <img src="img/main/painting.svg" alt="покраска">
                        </div>
                        <p>Профессиональные инструменты и технологии ремонта</p>
                    </li>
                </ul>
            </div>
            <img src="img/waves.svg" alt="волна">
            <?php
            include "form.php";
            ?>
        </div>
    </section>
    <?php
    include "services-section.php";
    ?>
    <section id="main-3">
        <div class="wrapper">
            <h1>Немного о нас</h1>
            <img src="img/waves.svg" alt="волны">
            <p>Мы являемся одними из ведущих авторемонтных мастерских, обслуживающих клиентов в Мариуполе.</p>
            <p>Все предоставляемые нами услуги выполняются высококвалифицированными специалистами.</p>
            <ul>
                <li>
                    <div class="shadow">
                        <div>
                            <img src="img/main/from-mouth-to-mouth.svg" alt="">
                        </div>
                    </div>
                    <h1>Персональный подход</h1>
                    <hr>
                    <p>Если Вы хотите качество ожидаемое от дилерской сети, но с более личной и дружественной атмосфере, то Вы обратились по адресу.</p>
                </li>
                <li>
                    <div class="shadow">
                        <div>
                            <img src="img/main/reparation.svg" alt="">
                        </div>
                    </div>
                    <h1>Лучшие материалы</h1>
                    <hr>
                    <p>Наша автомастерская обслуживает любые модели авто. Мы делаем только ту работу, которая необходима для устранения Вашей проблемы.</p>
                </li>
                <li>
                    <div class="shadow">
                        <div>
                            <img src="img/main/maintenance.svg" alt="">
                        </div>
                    </div>
                    <h1>Профессиональные стандарты</h1>
                    <hr>
                    <p>Наша автомастерская обслуживает любые модели авто. Мы делаем только ту работу, которая необходима для устранения Вашей проблемы.</p>
                </li>
            </ul>
            <a href="about">Узнать больше</a>
        </div>
    </section>
    <?php
    include "three-services.php";
    ?>
    <section id="main-5">
        <div class="wrapper">
            <div class="left">
                <img src="img/wave.svg" alt="волна">
                <h1>«Элит Авто», Мариуполь</h1>
                <p>Мы готовы предложить Вам автосервис в Мариуполе, индивидуальный подход к каждому клиенту. Следует отметить, что специалисты нашего автосервиса очень высокого уровня и применяют современные виды ремонтного оборудования для проведения всех видов работ в соответствии с рекомендациями завода-производителя</p>
                <p>Автовладелец гарантированно получает качественные, выполненные в сроки услуги.</p>
                <p>Наиболее популярные услуги - <a href="services?id=1">техническое обслуживание</a>, <a href="services?id=1">автомойка</a>
                    , а так же <a href="services?id=1">малярные работы</a>.</p>
            </div>
            <div class="right">
                <div>
                    <div>
                        <h1>График работы</h1>
                        <img src="img/main/clock.svg" alt="часы">
                    </div>
                    <p>
                        <span>Понедельник</span>
                        <span>8:00 - 17:00</span>
                    </p>
                    <p>
                        <span>Вторник</span>
                        <span>8:00 - 17:00</span>
                    </p>
                    <p>
                        <span>Среда</span>
                        <span>8:00 - 17:00</span>
                    </p>
                    <p>
                        <span>Четверг</span>
                        <span>8:00 - 17:00</span>
                    </p>
                    <p>
                        <span>Пятница</span>
                        <span>8:00 - 17:00</span>
                    </p>
                    <p>
                        <span>Суббота</span>
                        <span>8:00 - 12:00</span>
                    </p>
                    <p>
                        <span>Воскресенье</span>
                        <span>Выходной</span>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="main-6">
        <?php if($db = new PDO(
    "mysql:dbname=".$database_config["dbname"].
    ";host=".$database_config["host"],
    $database_config["username"],
    $database_config["password"])): ?>
        <div class="wrapper">
            <h1>Отзывы наших клиентов</h1>
            <div>
                <img id="bs-left-arrow" src="img/main/bs-left-arrow.svg" alt="стрелка влево">
                <?php
                $getAll = $db->prepare("
SELECT * 
FROM `benefits` 
WHERE `visible` = '1'
ORDER BY `date` DESC 
LIMIT 0, 5");
                $getAll->execute();
                $data = $getAll->fetchAll();
                foreach ($data as $row): ?>
                <div class="bs-slide">
                    <div class="stars">
                        <?php
                        for ($i = 0; $i < 5; $i++):
                        if($i <= $row["estimate"]):
                        ?>
                        <img src="img/star.svg" alt="звезда">
                        <?php else: ?>
                        <img src="img/star-empty.svg" alt="пустая звезда">
                        <?php
                        endif;
                        endfor;
                        ?>
                    </div>
                    <h2><?=$row["header"]?></h2>
                    <p><?=$row["benefit"]?></p>
                    <div class="name-date">
                        <span><?=date("d.m.Y", strtotime($row["date"]))?></span>
                        <p><?=$row["name"]?></p>
                    </div>
                </div>
                <?php endforeach; ?>
                <img id="bs-right-arrow" src="img/main/bs-right-arrow.svg" alt="стрелка вправо">
            </div>
            <a href="benefits#benefits-2">Все отзывы</a>
        </div>
        <?php endif; ?>
    </section>
    <section id="main-7">
        <div class="wrapper">
            <?php
            $data = json_decode(file_get_contents("img/marks/marks.json"), true);
            $is_not_empty = false;
            foreach ($data as $mark) {
                if($mark["selected"]) {
                    $is_not_empty = true;
                    break;
                }
            }
            if($is_not_empty):
            ?>
            <div class="top">
                <h1>Ремонт и обслуживание любых марок автомобилей</h1>
                <ul>
                    <?php
                    foreach ($data as $row):
                        if(!$row["selected"]) continue;
                            ?>
                            <li>
                                <img src="img/marks/<?=$row["photo"]?>.jpg" alt="<?=$row["mark-name"]?>">
                                <span><?=$row["mark-name"]?></span>
                            </li>
                        <?php
                    endforeach;
                    ?>
                </ul>
                <a>Смотреть все</a>
            </div>
            <?php endif; ?>
            <?php
            $questions_data = json_decode(file_get_contents("data/questions.json"), true);
            $count = count($questions_data);

            if($count > 0):
            ?>
            <div class="bottom">
                <h1>Часто задаваемые вопросы</h1>
                <ul>
                    <?php
                    for ($i = 0; $i < $count; $i++):
                        $header = $questions_data[$i]["header"];
                        $content = $questions_data[$i]["content"];
                        $incr = $i + 1;
                    ?>
                    <li>
                        <div>
                            <div class="tab-head">
                                <h2><?=$header?></h2>
                            </div>
                            <div class="content"><?=$content?></div>
                        </div>
                    </li>
                    <?php endfor; ?>
                </ul>
            </div>
            <?php else: endif; ?>
        </div>
        <?php
        include "map.php";
        ?>
    </section>
</article>
<?php
include "footer.php";
?>